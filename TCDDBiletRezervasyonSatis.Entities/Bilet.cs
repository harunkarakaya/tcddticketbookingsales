﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCDDBiletRezervasyonSatis.Entities;

namespace TCDDBiletRezervasyonSatis
{   
    [Table("Bilet")]
    public class Bilet
    {
        public Bilet()
        {
            //hat = new Hat();
            yon = new Yon();
            odeme = new Odeme();

            BiletPNRNO += 1;

        }

        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required,StringLength(50)]
        public BiletTipleri BiletTipi { get; set; }

        [Required]
        public string KalkisNoktasi { get; set; }

        [Required]
        public string VarisNoktasi { get; set; }

        [Required]
        public DateTime GidisTarihi { get; set; }

        [Required]
        public DateTime DonusTarihi { get; set; }

        public double BiletPuani { get; set; }
        public float HakEdilenIndirim { get; set; }
        public double ToplamTutar { get; set; }

        public virtual Hat hat { get; set; }
        public virtual Odeme odeme { get; set; }
        public virtual Yon yon { get; set; }
        public BiletDolulukTipi DolulukTipi { get; set; }
        public int KullaniciID { get; set; }
        public virtual Kullanici Kullanici { get; set; }
        public virtual Yolcu Yolcu { get; set; }


        private long biletpnrno = 0;
        public long BiletPNRNO
        {
            get
            {
                return BiletPNRNO;
            }
            set
            {
                biletpnrno = value;
            }
        }

        public double BiletPuaniHesapla(double BiletFiyati)
        {
            return BiletFiyati * 0.1; //Bilet fiyatının %10'unu puan olarak veriyoruz.
        }
    }
}
