﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCDDBiletRezervasyonSatis.Entities
{
    [Table("Tren")]
    public class Tren
    {
        public Tren(int trenkoltuksayisi,DateTime trenkalkisSaati)
        {
            this.TrenKalkisSaati = trenkalkisSaati;

            TrenKoltuklari = new List<TrenKoltugu>();

            string kod = "TCDD-";
            kod += DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();
            this.TrenNo = kod;
        }

        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        private string TrenNo { get; set; }
        public DateTime TrenKalkisSaati { get; set; }
        public virtual List<TrenKoltugu> TrenKoltuklari { get; set; }
        public int TrenKoltukSayisi { get; set; }
        public virtual TrenSeferi TrenSeferi { get; set; }
        public int YolcuSayisi { get; set; }
        public virtual List<Yolcu> Yolcular { get; set; }
    }
}
