﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCDDBiletRezervasyonSatis.Entities
{
    [Table("Kullanıcı")]
    public class Kullanici : Kisi
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required,StringLength(30)]
        public string KullaniciAdi { get; set; }

        [Required,StringLength(20)]
        public string Sifre { get; set; }

        public string EmailAdresi { get; set; }

        public virtual Profilim Profilim { get; set; }
    }
}
