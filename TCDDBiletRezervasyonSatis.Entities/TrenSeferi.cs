﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCDDBiletRezervasyonSatis.Entities
{
    [Table("TrenSeferleri")]
    public class TrenSeferi
    {
        public TrenSeferi()
        {
            Trenler = new List<Tren>();
        }

        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public virtual List<Tren> Trenler { get; set; }
        public DateTime SeferlerinTarihi { get; set; }
        public virtual Hat TrenSeferiHatti { get; set; }
    }
}
