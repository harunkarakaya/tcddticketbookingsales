﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCDDBiletRezervasyonSatis.Entities
{
    [Table("TrenKoltuğu")]
    public class TrenKoltugu
    {

        public TrenKoltugu(int trendekikoltugunsirasi)
        {
            //TO:DO koltuknolar 
            this.TrendekiKoltugunSirasi = trendekikoltugunsirasi;

            string[] alfabe = new string[] {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","R","S","T"};

            double bolum = 20;

            double bulunduguharf = Math.Floor(TrendekiKoltugunSirasi / bolum);
            double bulunduguSayi = trendekikoltugunsirasi - (bulunduguharf * bolum);
            
            TrenKoltukNo = alfabe[Convert.ToInt32(bulunduguharf)].ToString();
            TrenKoltukNo += bulunduguSayi.ToString();
        }

        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string TrenKoltukNo { get; set; }
        public int TrendekiKoltugunSirasi { get; set; }
        public bool KoltukBosDoluMu { get; set; }
        public virtual Tren Tren { get; set; }

    }
}
