﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCDDBiletRezervasyonSatis
{
    public class Hat
    {
        public string HatAdi { get; set; }
        public double HatFiyati { get; set; }

        public static List<Hat> hatlar = new List<Hat>
        {
            new Hat{HatAdi="İstanbul-İzmir",HatFiyati=70},
            new Hat{HatAdi="İstanbul-Ankara",HatFiyati=50},
            new Hat{HatAdi="İstanbul-Konya",HatFiyati=90},
            new Hat{HatAdi="İstanbul-Antalya",HatFiyati=130},

            new Hat{HatAdi="İzmir-Ankara",HatFiyati=60},
            new Hat{HatAdi="İzmir-Konya",HatFiyati=85},
            new Hat{HatAdi="İzmir-Antalya",HatFiyati=70},

            new Hat{HatAdi="Ankara-Konya",HatFiyati=30},
            new Hat{HatAdi="Ankara-Antalya",HatFiyati=50},

            new Hat{HatAdi="Konya-Antalya",HatFiyati=32}

        };
    }
}
