﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCDDBiletRezervasyonSatis.Entities
{
    [Table("Profilim")]
    public class Profilim
    {
        public Profilim()
        {
            Biletlerim = new List<Bilet>();
        }

        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public virtual List<Bilet> Biletlerim { get; set; }
        public double Puanlarim { get; set; }
        public virtual Kullanici Kullanici { get; set; }
    }
}
