﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCDDBiletRezervasyonSatis.Entities;

namespace TCDDBiletRezervasyonSatis
{
    [Table("Yolcu")]
    public class Yolcu : Kisi
    {
        public Yolcu()
        {
            bilet = new Bilet();
        }

        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        //public string YolcuTipi { get; set; }
        //public int YolcuSayisi { get; set; }
        public virtual Bilet bilet { get; set; }
        public virtual Kullanici Kullanici { get; set; }
    }
}
