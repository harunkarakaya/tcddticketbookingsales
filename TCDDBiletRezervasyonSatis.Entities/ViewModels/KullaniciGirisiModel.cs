﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCDDBiletRezervasyonSatis.Entities.ViewModels
{
    public class KullaniciGirisiModel
    {
        public string KullaniciAdi { get; set; }
        public string Sifre { get; set; }
    }
}
