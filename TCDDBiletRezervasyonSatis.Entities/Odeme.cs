﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCDDBiletRezervasyonSatis.Entities;

namespace TCDDBiletRezervasyonSatis
{
    [Table("Ödeme")]
    public class Odeme
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public OdemeTipleri OdemeTipi { get; set; }
        public double ToplamTutar { get; set; }
        public string KartBilgileri { get; set; }
        public DateTime KartSKT { get; set; }
        public string KartCVC { get; set; }
        public int TaksitSayisi { get; set; }

        public virtual Kullanici Kullanici { get; set; }

    }
}