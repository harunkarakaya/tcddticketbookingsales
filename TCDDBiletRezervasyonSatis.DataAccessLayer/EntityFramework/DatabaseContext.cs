﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCDDBiletRezervasyonSatis.DataAccessLayer.EntityFramework
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Bilet> Bilet { get; set; }
        public DbSet<Hat> Hat { get; set; }
        public DbSet<Odeme> Odeme { get; set; }
        public DbSet<Yolcu> Yolcu { get; set; }
        public DbSet<Yon> Yon { get; set; }
    }
}
