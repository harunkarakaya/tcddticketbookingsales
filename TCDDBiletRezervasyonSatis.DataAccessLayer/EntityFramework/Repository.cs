﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TCDDBiletRezervasyonSatis.Interfaces;

namespace TCDDBiletRezervasyonSatis.DataAccessLayer.EntityFramework
{
    public class Repository<T> : Singleton, IRepository<T> where T : class
    {
        private DbSet<T> objectSet;

        public Repository()
        {
            objectSet = context.Set<T>();
        }

        public int Delete(T obj)
        {
            objectSet.Remove(obj);

            return Save();
        }

        public T Find(Expression<Func<T, bool>> where)
        {
            throw new NotImplementedException();
        }

        public int Insert(T obj)
        {
            objectSet.Add(obj);

            return Save();
        }

        public List<T> List()
        {
            return objectSet.ToList();
        }

        public List<T> List(Expression<Func<T, bool>> where)
        {
            return objectSet.Where(where).ToList();
        }

        public IQueryable<T> ListQueryable()
        {
            throw new NotImplementedException();
        }

        public int Save()
        {
            return context.SaveChanges();
        }

        public int Update(T obj)
        {
            return Save();
        }
    }
}
