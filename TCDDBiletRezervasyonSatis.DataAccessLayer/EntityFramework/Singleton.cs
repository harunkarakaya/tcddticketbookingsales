﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCDDBiletRezervasyonSatis.DataAccessLayer.EntityFramework
{
    public class Singleton
    {
        protected static DatabaseContext context { get; set; }
        protected static object _lock = new object();

        public Singleton()
        {
            CreateContext();
        }

        public static void CreateContext()
        {
            if(context == null)
            {
                lock (_lock)
                {
                    if(context == null)
                    {
                        context = new DatabaseContext();
                    }
                }
            }
        }
    }
}
