﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCDDBiletRezervasyonSatis.Entities;

namespace TCDDBiletRezervasyonSatis.DataAccessLayer.EntityFramework
{
    public class MyInitializer : CreateDatabaseIfNotExists<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            Kullanici kullanici = new Kullanici();

            kullanici.Ad = "Harun";
            kullanici.Soyad = "KARAKAYA";
            kullanici.KullaniciAdi = "harun";
            kullanici.Sifre = "12345";
            kullanici.TCKNo = "1111111111";

            Profilim p = new Profilim();
            p.Kullanici = kullanici;
            p.Puanlarim = 47.65;
            //p.Biletlerim
            kullanici.Profilim = p;



            Bilet bilet = new Bilet();
            bilet.GidisTarihi = DateTime.Now;
            bilet.KalkisNoktasi = "İstanbul";
            bilet.VarisNoktasi = "Konya";

            foreach (Hat h in Hat.hatlar)
            {
                if (h.HatAdi.Contains(bilet.KalkisNoktasi) && h.HatAdi.Contains(bilet.VarisNoktasi))
                {
                    bilet.hat.HatAdi = h.HatAdi;
                    bilet.hat.HatFiyati = h.HatFiyati;
                }
            }


            Yolcu y = new Yolcu();
            y.bilet = bilet;
            y.Ad = kullanici.Ad;
            y.Soyad = kullanici.Soyad;
            y.TCKNo = kullanici.TCKNo;
            y.Kullanici = kullanici;
            y.DogumTarihi = Convert.ToDateTime("1995-05-07");
            y.bilet = bilet;


            bilet.Yolcu = y;
            bilet.BiletPuani = bilet.BiletPuaniHesapla(bilet.hat.HatFiyati);
            //bilet.
        }
    }
}
