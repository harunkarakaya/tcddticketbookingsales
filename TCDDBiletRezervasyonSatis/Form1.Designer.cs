﻿namespace TCDDBiletRezervasyonSatis
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rdbtnTam = new System.Windows.Forms.RadioButton();
            this.rdBtnOgrenci = new System.Windows.Forms.RadioButton();
            this.rdBtnTekYon = new System.Windows.Forms.RadioButton();
            this.rdBtnGidisDonusYon = new System.Windows.Forms.RadioButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.dTPickerGidisTarihi = new System.Windows.Forms.DateTimePicker();
            this.dTPickerDonusTarihi = new System.Windows.Forms.DateTimePicker();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblToplamTutar = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.numUpDownYolcuSayisi = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownYolcuSayisi)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bilet Tipi";
            // 
            // rdbtnTam
            // 
            this.rdbtnTam.AutoSize = true;
            this.rdbtnTam.Location = new System.Drawing.Point(2, 20);
            this.rdbtnTam.Name = "rdbtnTam";
            this.rdbtnTam.Size = new System.Drawing.Size(46, 17);
            this.rdbtnTam.TabIndex = 1;
            this.rdbtnTam.TabStop = true;
            this.rdbtnTam.Text = "Tam";
            this.rdbtnTam.UseVisualStyleBackColor = true;
            // 
            // rdBtnOgrenci
            // 
            this.rdBtnOgrenci.AutoSize = true;
            this.rdBtnOgrenci.Location = new System.Drawing.Point(54, 20);
            this.rdBtnOgrenci.Name = "rdBtnOgrenci";
            this.rdBtnOgrenci.Size = new System.Drawing.Size(62, 17);
            this.rdBtnOgrenci.TabIndex = 2;
            this.rdBtnOgrenci.TabStop = true;
            this.rdBtnOgrenci.Text = "Öğrenci";
            this.rdBtnOgrenci.UseVisualStyleBackColor = true;
            // 
            // rdBtnTekYon
            // 
            this.rdBtnTekYon.AutoSize = true;
            this.rdBtnTekYon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rdBtnTekYon.Location = new System.Drawing.Point(6, 13);
            this.rdBtnTekYon.Name = "rdBtnTekYon";
            this.rdBtnTekYon.Size = new System.Drawing.Size(73, 17);
            this.rdBtnTekYon.TabIndex = 4;
            this.rdBtnTekYon.TabStop = true;
            this.rdBtnTekYon.Text = "Tek Yön";
            this.rdBtnTekYon.UseVisualStyleBackColor = true;
            this.rdBtnTekYon.CheckedChanged += new System.EventHandler(this.rdBtnTekYon_CheckedChanged);
            // 
            // rdBtnGidisDonusYon
            // 
            this.rdBtnGidisDonusYon.AutoSize = true;
            this.rdBtnGidisDonusYon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rdBtnGidisDonusYon.Location = new System.Drawing.Point(85, 13);
            this.rdBtnGidisDonusYon.Name = "rdBtnGidisDonusYon";
            this.rdBtnGidisDonusYon.Size = new System.Drawing.Size(93, 17);
            this.rdBtnGidisDonusYon.TabIndex = 5;
            this.rdBtnGidisDonusYon.TabStop = true;
            this.rdBtnGidisDonusYon.Text = "Gidiş-Dönüş";
            this.rdBtnGidisDonusYon.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "İstanbul",
            "Ankara",
            "İzmir",
            "Konya",
            "Antalya"});
            this.comboBox1.Location = new System.Drawing.Point(15, 113);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 6;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(12, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Nereden";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(216, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nereye";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "İstanbul",
            "Ankara",
            "İzmir",
            "Konya",
            "Antalya"});
            this.comboBox2.Location = new System.Drawing.Point(219, 113);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 9;
            // 
            // dTPickerGidisTarihi
            // 
            this.dTPickerGidisTarihi.Location = new System.Drawing.Point(15, 203);
            this.dTPickerGidisTarihi.Name = "dTPickerGidisTarihi";
            this.dTPickerGidisTarihi.Size = new System.Drawing.Size(150, 20);
            this.dTPickerGidisTarihi.TabIndex = 10;
            // 
            // dTPickerDonusTarihi
            // 
            this.dTPickerDonusTarihi.Location = new System.Drawing.Point(219, 203);
            this.dTPickerDonusTarihi.Name = "dTPickerDonusTarihi";
            this.dTPickerDonusTarihi.Size = new System.Drawing.Size(151, 20);
            this.dTPickerDonusTarihi.TabIndex = 10;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radioButton1.Location = new System.Drawing.Point(15, 38);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton1.Size = new System.Drawing.Size(53, 17);
            this.radioButton1.TabIndex = 11;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Satış";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radioButton2.Location = new System.Drawing.Point(74, 38);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton2.Size = new System.Drawing.Size(98, 17);
            this.radioButton2.TabIndex = 12;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Rezervasyon";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(14, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Gidiş Tarihi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(218, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Dönüş Tarihi";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(10, 340);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "Yolcu Sayısı:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(14, 408);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Toplam Tutar:";
            // 
            // lblToplamTutar
            // 
            this.lblToplamTutar.AutoSize = true;
            this.lblToplamTutar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblToplamTutar.Location = new System.Drawing.Point(130, 408);
            this.lblToplamTutar.Name = "lblToplamTutar";
            this.lblToplamTutar.Size = new System.Drawing.Size(0, 17);
            this.lblToplamTutar.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(165, 461);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 44);
            this.button1.TabIndex = 20;
            this.button1.Text = "Hesapla";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numUpDownYolcuSayisi
            // 
            this.numUpDownYolcuSayisi.Location = new System.Drawing.Point(117, 340);
            this.numUpDownYolcuSayisi.Name = "numUpDownYolcuSayisi";
            this.numUpDownYolcuSayisi.Size = new System.Drawing.Size(67, 20);
            this.numUpDownYolcuSayisi.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdBtnTekYon);
            this.groupBox1.Controls.Add(this.rdBtnGidisDonusYon);
            this.groupBox1.Location = new System.Drawing.Point(219, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(200, 43);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.rdbtnTam);
            this.groupBox2.Controls.Add(this.rdBtnOgrenci);
            this.groupBox2.Location = new System.Drawing.Point(15, 262);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox2.Size = new System.Drawing.Size(139, 54);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 530);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.numUpDownYolcuSayisi);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblToplamTutar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.dTPickerDonusTarihi);
            this.Controls.Add(this.dTPickerGidisTarihi);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Name = "Form1";
            this.Text = "TCDD Bilet Satış-Rezevasyon Sistemi";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownYolcuSayisi)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdbtnTam;
        private System.Windows.Forms.RadioButton rdBtnOgrenci;
        private System.Windows.Forms.RadioButton rdBtnTekYon;
        private System.Windows.Forms.RadioButton rdBtnGidisDonusYon;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.DateTimePicker dTPickerGidisTarihi;
        private System.Windows.Forms.DateTimePicker dTPickerDonusTarihi;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblToplamTutar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown numUpDownYolcuSayisi;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

