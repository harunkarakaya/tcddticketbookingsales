﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCDDBiletRezervasyonSatis
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        Yolcu yolcu = new Yolcu();


        List<Hat> hatlar = new List<Hat>
        {
            new Hat{HatAdi="İstanbul-İzmir",HatFiyati=70},
            new Hat{HatAdi="İstanbul-Ankara",HatFiyati=50},
            new Hat{HatAdi="İstanbul-Konya",HatFiyati=90},
            new Hat{HatAdi="İstanbul-Antalya",HatFiyati=130},

            new Hat{HatAdi="İzmir-Ankara",HatFiyati=60},
            new Hat{HatAdi="İzmir-Konya",HatFiyati=85},
            new Hat{HatAdi="İzmir-Antalya",HatFiyati=70},

            new Hat{HatAdi="Ankara-Konya",HatFiyati=30},
            new Hat{HatAdi="Ankara-Antalya",HatFiyati=50},

            new Hat{HatAdi="Konya-Antalya",HatFiyati=32}

        };

        private void Form1_Load(object sender, EventArgs e)
        {
            dTPickerDonusTarihi.Enabled = false;
            dTPickerGidisTarihi.Enabled = false;

            string Nereden = "İstanbul";
            string Nereye = "İzmir";
            comboBox1.Text = Nereden;
            comboBox2.Text = Nereye;

            rdbtnTam.Checked = true;
            rdBtnTekYon.Checked = true;
         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Nereden = "";
            string Nereye = "";

            if(numUpDownYolcuSayisi.Text != null && numUpDownYolcuSayisi.Text != "")
            {
                yolcu.YolcuSayisi = (int)numUpDownYolcuSayisi.Value;
            }

            if (comboBox1.SelectedItem != null)
            {
                Nereden = comboBox1.SelectedItem.ToString();
            }

            if (comboBox2.SelectedItem != null)
            {
                Nereye = comboBox2.SelectedItem.ToString();
            }

            if(rdBtnTekYon.Checked == true)
            {
                yolcu.bilet.yon.YonSecenegi = YonSecenekleri.Tek;
            }

            if(rdBtnGidisDonusYon.Checked == true)
            {
                yolcu.bilet.yon.YonSecenegi = YonSecenekleri.GidisDonus;
            }

            foreach (Hat h in hatlar)
            {
                if (h.HatAdi.Contains(Nereden) && h.HatAdi.Contains(Nereye))
                {
                    //yolcu.bilet.ToplamTutar = h.HatFiyati;
                    yolcu.bilet.hat.HatFiyati = h.HatFiyati;
                }
            }

            if(rdBtnOgrenci.Checked == true)
            {
                yolcu.bilet.BiletTipi = BiletTipleri.Ogrenci;
            }
            
            if(rdbtnTam.Checked == true)
            {
                yolcu.bilet.BiletTipi = BiletTipleri.Tam;
            }


            BiletToplamTutarHesapla(yolcu);
        }

        private void rdBtnTekYon_CheckedChanged(object sender, EventArgs e)
        {
            if(rdBtnTekYon.Checked == true)
            {
                dTPickerGidisTarihi.Enabled = true;
                dTPickerDonusTarihi.Enabled = false;
            }

            if (rdBtnGidisDonusYon.Checked == true)
            {
                dTPickerDonusTarihi.Enabled = true;
                dTPickerGidisTarihi.Enabled = true;
            }
        }

        public void BiletToplamTutarHesapla(Yolcu yolcuBilgileri)
        {
            double indirim=1;
            int ciftBilet=1;

            if(yolcu.bilet.BiletTipi == BiletTipleri.Ogrenci)
            {
                indirim = 0.80;
            }

            if(yolcu.bilet.yon.YonSecenegi == YonSecenekleri.GidisDonus)
            {
                ciftBilet = 2;
            }

            yolcu.bilet.ToplamTutar = yolcu.bilet.hat.HatFiyati * yolcu.YolcuSayisi * indirim * ciftBilet;

            lblToplamTutar.Text = yolcu.bilet.ToplamTutar.ToString() + " TL";
        }
    }
}
